<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DetalleReserva;

/**
 * DetalleReservaSearch represents the model behind the search form of `app\models\DetalleReserva`.
 */
class DetalleReservaSearch extends DetalleReserva
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_detalle_reserva', 'id_reserva', 'id_butaca'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DetalleReserva::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_detalle_reserva' => $this->id_detalle_reserva,
            'id_reserva' => $this->id_reserva,
            'id_butaca' => $this->id_butaca,
        ]);

        return $dataProvider;
    }
}
