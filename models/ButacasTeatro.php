<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "butacas_teatro".
 *
 * @property int $id_butaca
 * @property string $columna
 * @property string $fila
 * @property int $disponible
 * @property int $id_obra
 *
 * @property ObraTeatro $obra
 * @property DetalleReserva[] $detalleReservas
 */
class ButacasTeatro extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'butacas_teatro';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_butaca'], 'required'],
            [['id_butaca', 'disponible', 'id_obra'], 'integer'],
            [['columna'], 'string', 'max' => 2],
            [['fila'], 'string', 'max' => 1],
            [['id_butaca'], 'unique'],
            [['id_obra'], 'exist', 'skipOnError' => true, 'targetClass' => ObraTeatro::className(), 'targetAttribute' => ['id_obra' => 'id_obra']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_butaca' => 'Id Butaca',
            'columna' => 'Columna',
            'fila' => 'Fila',
            'disponible' => 'Disponible',
            'id_obra' => 'Id Obra',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObra()
    {
        return $this->hasOne(ObraTeatro::className(), ['id_obra' => 'id_obra']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalleReservas()
    {
        return $this->hasMany(DetalleReserva::className(), ['id_butaca' => 'id_butaca']);
    }
}
