<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ObraTeatro;

/**
 * ObraTeatroSearch represents the model behind the search form of `app\models\ObraTeatro`.
 */
class ObraTeatroSearch extends ObraTeatro
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_obra'], 'integer'],
            [['nombre_obra', 'fecha', 'Hora'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ObraTeatro::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_obra' => $this->id_obra,
            'fecha' => $this->fecha,
        ]);

        $query->andFilterWhere(['like', 'nombre_obra', $this->nombre_obra])
            ->andFilterWhere(['like', 'Hora', $this->Hora]);

        return $dataProvider;
    }
}
