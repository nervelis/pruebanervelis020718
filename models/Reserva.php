<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reserva".
 *
 * @property int $id_reserva
 * @property string $fecha_reserva
 * @property int $id_obra
 * @property int $id_usuario
 *
 * @property DetalleReserva[] $detalleReservas
 * @property ObraTeatro $obra
 * @property Usuario $usuario
 */
class Reserva extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reserva';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_reserva'], 'required'],
            [['id_reserva', 'id_obra', 'id_usuario'], 'integer'],
            [['fecha_reserva'], 'safe'],
            [['id_reserva'], 'unique'],
            [['id_obra'], 'exist', 'skipOnError' => true, 'targetClass' => ObraTeatro::className(), 'targetAttribute' => ['id_obra' => 'id_obra']],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['id_usuario' => 'id_usuario']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_reserva' => 'Id Reserva',
            'fecha_reserva' => 'Fecha Reserva',
            'id_obra' => 'Id Obra',
            'id_usuario' => 'Id Usuario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalleReservas()
    {
        return $this->hasMany(DetalleReserva::className(), ['id_reserva' => 'id_reserva']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObra()
    {
        return $this->hasOne(ObraTeatro::className(), ['id_obra' => 'id_obra']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id_usuario' => 'id_usuario']);
    }
}
