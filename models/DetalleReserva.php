<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "detalle_reserva".
 *
 * @property int $id_detalle_reserva
 * @property int $id_reserva
 * @property int $id_butaca
 *
 * @property ButacasTeatro $butaca
 * @property Reserva $reserva
 */
class DetalleReserva extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'detalle_reserva';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_detalle_reserva'], 'required'],
            [['id_detalle_reserva', 'id_reserva', 'id_butaca'], 'integer'],
            [['id_detalle_reserva'], 'unique'],
            [['id_butaca'], 'exist', 'skipOnError' => true, 'targetClass' => ButacasTeatro::className(), 'targetAttribute' => ['id_butaca' => 'id_butaca']],
            [['id_reserva'], 'exist', 'skipOnError' => true, 'targetClass' => Reserva::className(), 'targetAttribute' => ['id_reserva' => 'id_reserva']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_detalle_reserva' => 'Id Detalle Reserva',
            'id_reserva' => 'Id Reserva',
            'id_butaca' => 'Id Butaca',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getButaca()
    {
        return $this->hasOne(ButacasTeatro::className(), ['id_butaca' => 'id_butaca']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReserva()
    {
        return $this->hasOne(Reserva::className(), ['id_reserva' => 'id_reserva']);
    }
}
