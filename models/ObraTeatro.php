<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "obra_teatro".
 *
 * @property int $id_obra
 * @property string $nombre_obra
 * @property string $fecha
 * @property string $Hora
 *
 * @property ButacasTeatro[] $butacasTeatros
 * @property Reserva[] $reservas
 */
class ObraTeatro extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'obra_teatro';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_obra'], 'required'],
            [['id_obra'], 'integer'],
            [['fecha'], 'safe'],
            [['nombre_obra'], 'string', 'max' => 120],
            [['Hora'], 'string', 'max' => 60],
            [['id_obra'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_obra' => 'Id Obra',
            'nombre_obra' => 'Nombre Obra',
            'fecha' => 'Fecha',
            'Hora' => 'Hora',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getButacasTeatros()
    {
        return $this->hasMany(ButacasTeatro::className(), ['id_obra' => 'id_obra']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReservas()
    {
        return $this->hasMany(Reserva::className(), ['id_obra' => 'id_obra']);
    }
}
