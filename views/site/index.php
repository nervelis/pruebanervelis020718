<?php

/* @var $this yii\web\View */

$this->title = 'Reserva Teatro Venezuela';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Sistema de Reserva del Teatro Venezuela</h1>

        <p class="lead">Bienvenido (a), para reservar debe ingresar con su usuario y clave</p>
        <p class="lead">Para efecto de la prueba, el usuario: admin y la clave:admin</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Reservar</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Consígueme una vida</h2>

                <p>¿Qué pasaría si en medio de una crisis de infelicidad tuviéramos la oportunidad de poner una pausa y vivir de otra manera? </p>
                <p>Musical, Comedia</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Reservar &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>El búho que le temía a la oscuridad</h2>

                <p>Úlu, es un pequeño búho que tiene miedo de la oscuridad pero con ayuda de su mamá y un par de nuevos amigos descubrirá que la noche trae consigo sorpresas.</p>

                <p>Infantil</p>


                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Reservar &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Todos los peces de la tierra</h2>

                <p>¿Hasta cuándo es correcto abandonar toda esperanza?<p>
                <p>Contemporáneo<p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Reservar &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
