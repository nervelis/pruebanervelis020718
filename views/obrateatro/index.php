<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ObraTeatroSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Obra Teatros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="obra-teatro-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Obra Teatro', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_obra',
            'nombre_obra',
            'fecha',
            'Hora',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
