<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ObraTeatro */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="obra-teatro-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_obra')->textInput() ?>

    <?= $form->field($model, 'nombre_obra')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'Hora')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
