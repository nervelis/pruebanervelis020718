<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ObraTeatroSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="obra-teatro-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_obra') ?>

    <?= $form->field($model, 'nombre_obra') ?>

    <?= $form->field($model, 'fecha') ?>

    <?= $form->field($model, 'Hora') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
