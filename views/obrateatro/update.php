<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ObraTeatro */

$this->title = 'Update Obra Teatro: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Obra Teatros', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_obra, 'url' => ['view', 'id' => $model->id_obra]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="obra-teatro-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
