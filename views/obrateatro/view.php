<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ObraTeatro */

$this->title = $model->id_obra;
$this->params['breadcrumbs'][] = ['label' => 'Obra Teatros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="obra-teatro-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_obra], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_obra], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_obra',
            'nombre_obra',
            'fecha',
            'Hora',
        ],
    ]) ?>

</div>
