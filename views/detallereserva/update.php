<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DetalleReserva */

$this->title = 'Update Detalle Reserva: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Detalle Reservas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_detalle_reserva, 'url' => ['view', 'id' => $model->id_detalle_reserva]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="detalle-reserva-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
