<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DetalleReserva */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="detalle-reserva-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_detalle_reserva')->textInput() ?>

    <?= $form->field($model, 'id_reserva')->textInput() ?>

    <?= $form->field($model, 'id_butaca')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
