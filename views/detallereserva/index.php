<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DetalleReservaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Detalle Reservas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="detalle-reserva-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Detalle Reserva', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_detalle_reserva',
            'id_reserva',
            'id_butaca',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
