<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ButacasTeatro */

$this->title = 'Create Butacas Teatro';
$this->params['breadcrumbs'][] = ['label' => 'Butacas Teatros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="butacas-teatro-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
