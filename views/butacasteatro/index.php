<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ButacasTeatroSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Butacas Teatros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="butacas-teatro-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Butacas Teatro', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_butaca',
            'columna',
            'fila',
            'disponible',
            'id_obra',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
