<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ButacasTeatro */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="butacas-teatro-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_butaca')->textInput() ?>

    <?= $form->field($model, 'columna')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fila')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'disponible')->textInput() ?>

    <?= $form->field($model, 'id_obra')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
