<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ButacasTeatroSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="butacas-teatro-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_butaca') ?>

    <?= $form->field($model, 'columna') ?>

    <?= $form->field($model, 'fila') ?>

    <?= $form->field($model, 'disponible') ?>

    <?= $form->field($model, 'id_obra') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
